local hex_binary_map = {
    ["0"] = "0000",
    ["1"] = "0001",
    ["2"] = "0010",
    ["3"] = "0011",
    ["4"] = "0100",
    ["5"] = "0101",
    ["6"] = "0110",
    ["7"] = "0111",
    ["8"] = "1000",
    ["9"] = "1001",
    ["A"] = "1010",
    ["B"] = "1011",
    ["C"] = "1100",
    ["D"] = "1101",
    ["E"] = "1110",
    ["F"] = "1111"
}

local rank_string_original = ExperienceManager.rank_string

function ExperienceManager:rank_string(rank, use_roman_numerals, ...)
    local is_card = use_roman_numerals ~= nil
    local convert = InfamyRankConverter:GetOption(is_card and "convert_cards" or "convert_normal")
    local result = rank_string_original(self, rank, use_roman_numerals, ...)
    if use_roman_numerals == nil then
        use_roman_numerals = managers.user:get_setting("infamy_roman_rank")
    end
    if not use_roman_numerals then
        if convert == 2 then
            return self:rank_to_binary(result)
        elseif convert == 3 then
            return self:rank_to_hex(result)
        end
    end
    return result
end

function ExperienceManager:rank_to_binary(rank)
    local result = ""
    local hex = self:rank_to_hex(rank)
    for i = 1, hex:len() do
        result = result .. hex_binary_map[hex:sub(i, i)] .. " "
    end
    return string.trim(result)
end

function ExperienceManager:rank_to_hex(rank)
    return string.format("%.2X", rank)
end
