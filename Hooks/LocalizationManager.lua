Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_InfamyRankConverter", function(loc)
    local language_filename

    for _, filename in pairs(file.GetFiles(InfamyRankConverter._path .. 'Loc/')) do
        local str = filename:match('^(.*).json$')
        if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
            language_filename = filename
            break
        end
    end

    if language_filename then
        loc:load_localization_file(InfamyRankConverter._path .. 'Loc/' .. language_filename)
    end
    loc:load_localization_file(InfamyRankConverter._path .. "Loc/english.json", false)
end)
