InfamyRankConverter = InfamyRankConverter or {}
if not InfamyRankConverter.setup then
    InfamyRankConverter.setup = true

    InfamyRankConverter._path = ModPath
    InfamyRankConverter._options_file = SavePath .. "InfamyRankConverter.json"
    InfamyRankConverter._options = {}

    function InfamyRankConverter:Save()
        local file = io.open(self._options_file, "w+")
        if file then
            file:write(json.encode(self._options))
            file:close()
        end
    end

    function InfamyRankConverter:Load()
        self:LoadDefaults()
        local file = io.open(self._options_file, "r")
        if file then
            local settings = json.decode(file:read("*all"))
            file:close()
            if settings ~= nil and type(settings) == "table" then
                for k, v in pairs(settings) do
                    self._options[k] = v
                end
            end
        end
    end

    function InfamyRankConverter:GetOption(id)
        return self._options[id]
    end

    function InfamyRankConverter:SetOption(id, value)
        if self._options[id] ~= value then
            self._options[id] = value
            self:OptionChanged()
        end
    end

    function InfamyRankConverter:OptionChanged()
        self:Save()
    end

    function InfamyRankConverter:LoadDefaults()
        local default_file = io.open(self._path .. "default_values.json")
        if default_file then
            self._options = json.decode(default_file:read("*all"))
            default_file:close()
        end
    end

    InfamyRankConverter:Load()
    MenuHelper:LoadFromJsonFile(InfamyRankConverter._path .. "Menu/menu.json", InfamyRankConverter,
        InfamyRankConverter._options)
end
